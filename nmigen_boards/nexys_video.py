import os
import subprocess

from nmigen.build import *
from nmigen.vendor.xilinx import *
from nmigen_boards.resources import *


class NexysVideoPlatform(XilinxPlatform):
    device      = "xc7a200t"
    package     = "sbg484"
    speed       = "1"
    default_clk = "clk100"
    default_rst = "rst"
    resources   = [
        Resource("clk100", 0, Pins("R4", dir="i"),
                 Clock(100e6), Attrs(IOSTANDARD="LVCMOS33")),
        Resource("rst", 0, PinsN("G4", dir="i"), Attrs(IOSTANDARD="LVCMOS15")),

        *LEDResources(pins="T14 T15 T16 U16 V15 W16 W15 Y13", attrs=Attrs(IOSTANDARD="LVCMOS25")),

        *ButtonResources(pins="B22 D22 C22 D14 F15", attrs=Attrs(IOSTANDARD="LVCMOS33")),
        *SwitchResources(pins="E22 F21 G21 G22 H17 J16 K13 M17", attrs=Attrs(IOSTANDARD="LVCMOS25"))
    ]
    connectors  = [
    ]

    def toolchain_prepare(self, fragment, name, **kwargs):
        overrides = {
            "script_before_bitstream":
                "set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4 [current_design]",
            "script_after_bitstream":
                "write_cfgmem -force -format bin -interface spix4 -size 16 "
                "-loadbit \"up 0x0 {name}.bit\" -file {name}.bin".format(name=name),
            "add_constraints":
                """
                set_property INTERNAL_VREF 0.750 [get_iobanks 35]
                set_property CFGBVS VCCO [current_design]
                set_property CONFIG_VOLTAGE 3.3 [current_design]
                """
        }
        return super().toolchain_prepare(fragment, name, **overrides, **kwargs)

    def toolchain_program(self, products, name):
        xc3sprog = os.environ.get("XC3SPROG", "xc3sprog")
        with products.extract("{}.bit".format(name)) as bitstream_filename:
            subprocess.run([xc3sprog, "-c", "nexys4", bitstream_filename], check=True)


if __name__ == "__main__":
    from nmigen_boards.test.blinky import *
    NexysVideoPlatform(toolchain="yosys_nextpnr").build(Blinky(),
                        do_program=True)
